# Dotnet Version Manager

## Intro

`dvm` allows you to quickly install and use different versions of dotnet via the command line.

**Example:**
```sh
$ dotnet use 7.0.404
Now using dotnet 7.0.404
$ dotnet --version
7.0.404
$ dotnet use 6.0.417
Now using dotnet 6.0.417
$ dotnet --version
6.0.417
```

Simple as that!

## Installing and Updating

To **install** or **update** nvm, you should run the [install script][2]. To do that, you may either download and run the script manually, or use the following cURL or Wget command:
```sh
curl -o- https://gitlab.com/fcavalieri/dvm/-/raw/main/install.sh?ref_type=heads | bash
```
```sh
wget -qO- https://gitlab.com/fcavalieri/dvm/-/raw/main/install.sh?ref_type=heads | bash
```

Running either of the above commands downloads a script and runs it. The downloads dvm repository to `~/.dvm`, and attempts to add the source lines from the snippet below to the correct profile file (`~/.bash_profile`, `~/.zshrc`, `~/.profile`, or `~/.bashrc`).

```sh
export DVM_DIR="$HOME/.dvm"
[[ -s "$DVM_DIR/dvm-init.sh" ]] && source "$DVM_DIR/dvm-init.sh"
```

## License

dvm reuses the installer script and part of the README.md from [nvm](https://github.com/nvm-sh/nvm).
See [LICENSE](./LICENSE).


#!/bin/bash

function __dvm_version() {
  echo "DVM version 0.0.4"
}

function __dvm_list() {
  echo "Installed versions:"
  while IFS='' read -r -d '' candidate; do
    echo $(basename "$candidate")
  done < <(find "$DVM_CANDIDATES_DIR" -mindepth 1 -maxdepth 1 -type d -print0)
}

function __dvm_list_remote() {  
  local releases_url="https://dotnetcli.azureedge.net/dotnet/release-metadata/releases-index.json"
  echo $(curl --silent "$releases_url" | jq --raw-output '."releases-index"[] | select([ ."product" == ".NET"] | any) | ."latest-sdk"')  
}

function __dvm_current() {
  if [ -L "$DVM_CURRENT_DIR" ]
  then
    basename $(readlink -f "$DVM_CURRENT_DIR")
  else
    echo "No current version set"
  fi
}

function __dvm_use() {
  local candidate_dir="$DVM_CANDIDATES_DIR/$1"
  if [ -d "$candidate_dir" ]
  then
    rm -f "$DVM_CURRENT_DIR"
    ln -s "$candidate_dir" "$DVM_CURRENT_DIR"
  echo "Now using dotnet $1"
  else
    echo "Version $1 is not installed."
    list
  fi
}

function __dvm_do_install() {
  local TEMPFILE=$(mktemp)
  local TEMPFOLDER=$(mktemp -d)
  echo "Downloading dotnet $1"
  wget -qO "$TEMPFILE" "https://dotnetcli.azureedge.net/dotnet/Sdk/$1/dotnet-sdk-$1-linux-x64.tar.gz"
  echo "Extracting dotnet $1"
  (
    cd "$TEMPFOLDER"
    tar xzf "$TEMPFILE"
  )
  rm -f "$TEMPFILE"
  echo "Installing dotnet $1"
  candidate_dir="$DVM_CANDIDATES_DIR/$1"
  rm -rf "$candidate_dir"
  mv "$TEMPFOLDER" "$candidate_dir"
  echo "Version $1 installed."
}

function __dvm_usage() {
  echo "Usage:"
  echo "dvm version            - Shows dvm version"
  echo "dvm list-remote        - Lists remote dotnet versions"
  echo "dvm list               - Lists installed dotnet versions"
  echo "dvm current            - Outputs the current dotnet version"
  echo "dvm use <VERSION>      - Uses an installed dotnet version"
  echo "dvm install <VERSION>  - Installs a dotnet version"
}

function dvm() {
  case "$1" in
    version)
      __dvm_version
      ;;

    list-remote)
      __dvm_list_remote
      ;;

    list)
      __dvm_list
      ;;

    current)
      __dvm_current
      ;;

    install)
      if [ "$#" -eq  "2" ]
      then
        __dvm_do_install "$2"
      else
        __dvm_usage
      fi
      ;;

    use)
      if [ "$#" -eq  "2" ]
      then
        __dvm_use "$2"
      else
        __dvm_usage
      fi
      ;;

    *)
      __dvm_usage
      ;;
  esac
}
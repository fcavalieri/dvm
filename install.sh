#!/usr/bin/env bash

{ # this ensures the entire script is downloaded #

dvm_has() {
  type "$1" > /dev/null 2>&1
}

dvm_echo() {
  command printf %s\\n "$*" 2>/dev/null
}

if [ -z "${BASH_VERSION}" ] || [ -n "${ZSH_VERSION}" ]; then
  # shellcheck disable=SC2016
  dvm_echo >&2 'Error: the install instructions explicitly say to pipe the install script to `bash`; please follow them'
  exit 1
fi

dvm_grep() {
  GREP_OPTIONS='' command grep "$@"
}

dvm_default_install_dir() {
  [ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.dvm" || printf %s "${XDG_CONFIG_HOME}/dvm"
}

dvm_install_dir() {
  if [ -n "$DVM_DIR" ]; then
    printf %s "${DVM_DIR}"
  else
    dvm_default_install_dir
  fi
}

dvm_profile_is_bash_or_zsh() {
  local TEST_PROFILE
  TEST_PROFILE="${1-}"
  case "${TEST_PROFILE-}" in
    *"/.bashrc" | *"/.bash_profile" | *"/.zshrc" | *"/.zprofile")
      return
    ;;
    *)
      return 1
    ;;
  esac
}

dvm_download() {
  if dvm_has "curl"; then
    curl --fail --compressed -q "$@"
  elif dvm_has "wget"; then
    # Emulate curl with wget
    ARGS=$(dvm_echo "$@" | command sed -e 's/--progress-bar /--progress=bar /' \
                            -e 's/--compressed //' \
                            -e 's/--fail //' \
                            -e 's/-L //' \
                            -e 's/-I /--server-response /' \
                            -e 's/-s /-q /' \
                            -e 's/-sS /-nv /' \
                            -e 's/-o /-O /' \
                            -e 's/-C - /-c /')
    # shellcheck disable=SC2086
    eval wget $ARGS
  fi
}

install_dvm_as_script() {
  local INSTALL_DIR
  INSTALL_DIR="$(dvm_install_dir)"
  local DVM_SCRIPT_SOURCE
  DVM_SCRIPT_SOURCE="https://gitlab.com/fcavalieri/dvm/-/raw/main/dvm.sh?ref_type=heads"  
  local DVM_INIT_SOURCE
  DVM_INIT_SOURCE="https://gitlab.com/fcavalieri/dvm/-/raw/main/dvm-init.sh?ref_type=heads"  

  # Downloading to $INSTALL_DIR  
  if [ -f "$INSTALL_DIR/dvm.sh" ]; then
    dvm_echo "=> dvm is already installed in $INSTALL_DIR, trying to update the script"
  else
    dvm_echo "=> Downloading dvm as script to '$INSTALL_DIR'"
  fi
  mkdir -p "$INSTALL_DIR"
  mkdir -p "$INSTALL_DIR/candidates"
  dvm_download -s "$DVM_SCRIPT_SOURCE" -o "$INSTALL_DIR/dvm.sh" || {
    dvm_echo >&2 "Failed to download '$DVM_SCRIPT_SOURCE'"
    return 1
  } &
  dvm_download -s "$DVM_INIT_SOURCE" -o "$INSTALL_DIR/dvm-init.sh" || {
    dvm_echo >&2 "Failed to download '$DVM_INIT_SOURCE'"
    return 1
  } &  
  for job in $(jobs -p | command sort)
  do
    wait "$job" || return $?
  done
  dvm_echo "=> Downloaded dvm as script to '$INSTALL_DIR'"
  chmod a+x "$INSTALL_DIR/dvm.sh" || {
    dvm_echo >&2 "Failed to mark '$INSTALL_DIR/dvm.sh' as executable"
    return 3
  }
}

dvm_try_profile() {
  if [ -z "${1-}" ] || [ ! -f "${1}" ]; then
    return 1
  fi
  dvm_echo "${1}"
}

#
# Detect profile file if not specified as environment variable
# (eg: PROFILE=~/.myprofile)
# The echo'ed path is guaranteed to be an existing file
# Otherwise, an empty string is returned
#
dvm_detect_profile() {
  if [ "${PROFILE-}" = '/dev/null' ]; then
    # the user has specifically requested NOT to have dvm touch their profile
    return
  fi

  if [ -n "${PROFILE}" ] && [ -f "${PROFILE}" ]; then
    dvm_echo "${PROFILE}"
    return
  fi

  local DETECTED_PROFILE
  DETECTED_PROFILE=''

  if [ "${SHELL#*bash}" != "$SHELL" ]; then
    if [ -f "$HOME/.bashrc" ]; then
      DETECTED_PROFILE="$HOME/.bashrc"
    elif [ -f "$HOME/.bash_profile" ]; then
      DETECTED_PROFILE="$HOME/.bash_profile"
    fi
  elif [ "${SHELL#*zsh}" != "$SHELL" ]; then
    if [ -f "$HOME/.zshrc" ]; then
      DETECTED_PROFILE="$HOME/.zshrc"
    elif [ -f "$HOME/.zprofile" ]; then
      DETECTED_PROFILE="$HOME/.zprofile"
    fi
  fi

  if [ -z "$DETECTED_PROFILE" ]; then
    for EACH_PROFILE in ".profile" ".bashrc" ".bash_profile" ".zprofile" ".zshrc"
    do
      if DETECTED_PROFILE="$(dvm_try_profile "${HOME}/${EACH_PROFILE}")"; then
        break
      fi
    done
  fi

  if [ -n "$DETECTED_PROFILE" ]; then
    dvm_echo "$DETECTED_PROFILE"
  fi
}

dvm_do_install() {
  if [ -n "${DVM_DIR-}" ] && ! [ -d "${DVM_DIR}" ]; then
    if [ -e "${DVM_DIR}" ]; then
      dvm_echo >&2 "File \"${DVM_DIR}\" has the same name as installation directory."
      exit 1
    fi

    if [ "${DVM_DIR}" = "$(dvm_default_install_dir)" ]; then
      mkdir "${DVM_DIR}"
    else
      dvm_echo >&2 "You have \$DVM_DIR set to \"${DVM_DIR}\", but that directory does not exist. Check your profile files and environment."
      exit 1
    fi
  fi
  # Disable the optional which check, https://www.shellcheck.net/wiki/SC2230
  # shellcheck disable=SC2230
  if dvm_has xcode-select && [ "$(xcode-select -p >/dev/null 2>/dev/null ; echo $?)" = '2' ] && [ "$(which git)" = '/usr/bin/git' ] && [ "$(which curl)" = '/usr/bin/curl' ]; then
    dvm_echo >&2 'You may be on a Mac, and need to install the Xcode Command Line Developer Tools.'
    # shellcheck disable=SC2016
    dvm_echo >&2 'If so, run `xcode-select --install` and try again. If not, please report this!'
    exit 1
  fi
  if dvm_has curl || dvm_has wget; then
      install_dvm_as_script
  else
    dvm_echo >&2 'You need git, curl, or wget to install dvm'
    exit 1
  fi
  
  dvm_echo

  local DVM_PROFILE
  DVM_PROFILE="$(dvm_detect_profile)"
  local PROFILE_INSTALL_DIR
  PROFILE_INSTALL_DIR="$(dvm_install_dir | command sed "s:^$HOME:\$HOME:")"

  SOURCE_STR="\\nexport DVM_DIR=\"${PROFILE_INSTALL_DIR}\"\\n[ -s \"\$DVM_DIR/dvm-init.sh\" ] && \\. \"\$DVM_DIR/dvm-init.sh\"  # This loads dvm\\n"

  BASH_OR_ZSH=false

  if [ -z "${DVM_PROFILE-}" ] ; then
    local TRIED_PROFILE
    if [ -n "${PROFILE}" ]; then
      TRIED_PROFILE="${DVM_PROFILE} (as defined in \$PROFILE), "
    fi
    dvm_echo "=> Profile not found. Tried ${TRIED_PROFILE-}~/.bashrc, ~/.bash_profile, ~/.zprofile, ~/.zshrc, and ~/.profile."
    dvm_echo "=> Create one of them and run this script again"
    dvm_echo "   OR"
    dvm_echo "=> Append the following lines to the correct file yourself:"
    command printf "${SOURCE_STR}"
    dvm_echo
  else
    if dvm_profile_is_bash_or_zsh "${DVM_PROFILE-}"; then
      BASH_OR_ZSH=true
    fi
    if ! command grep -qc '/dvm-init.sh' "$DVM_PROFILE"; then
      dvm_echo "=> Appending dvm source string to $DVM_PROFILE"
      command printf "${SOURCE_STR}" >> "$DVM_PROFILE"
    else
      dvm_echo "=> dvm source string already in ${DVM_PROFILE}"
    fi   
  fi

  # Source dvm
  # shellcheck source=/dev/null
  \. "$(dvm_install_dir)/dvm-init.sh"

  dvm_reset

  dvm_echo "=> Close and reopen your terminal to start using dvm or run the following to use it now:"
  command printf "${SOURCE_STR}"  
}

#
# Unsets the various functions defined
# during the execution of the install script
#
dvm_reset() {
  unset -f dvm_has dvm_install_dir dvm_latest_version dvm_profile_is_bash_or_zsh \
    dvm_source dvm_node_version dvm_download install_dvm_from_git dvm_install_node \
    install_dvm_as_script dvm_try_profile dvm_detect_profile dvm_check_global_modules \
    dvm_do_install dvm_reset dvm_default_install_dir dvm_grep
}

dvm_do_install

} # this ensures the entire script is downloaded #

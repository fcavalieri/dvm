if [ -z "$DVM_DIR" ]; then
  export DVM_DIR="$HOME/.dvm"
fi

export DVM_CANDIDATES_DIR="${DVM_DIR}/candidates"
export DVM_CURRENT_DIR="${DVM_DIR}/current"
export DOTNET_ROOT="$DVM_DIR/current"
export PATH="$DOTNET_ROOT:$DOTNET_ROOT/tools:$PATH"
source "$DVM_DIR/dvm.sh"